# TMx OnBoard bépo
**[en]**
A bépo layout for the virtual keyboard OnBoard, on linux.

*Installation :*
In OnBoard settings, go to the layout tab, click "Open layout folder" and there paste the tmx* files.
In the layouts list, the new layout called tmx shall have appeared.

**[fr]**
C'est d'ailleurs un peu stupide de ma part d'avoir mis des explications en anglais vu que c'est pour du bépo, mais bon.
Une disposition bépo pour le clavier virtuel OnBoard, sur linux. Cette disposition reprend celle du typematrix TMx 2030 USB.

*Installation :*
Dans les préférences d'OnBoard, choisissez l'onglet Agencement, cliquez sur "Ouvrir le dossier des agencements" et y coller les fichiers tmx*.
Dans la liste des agencements (quitte à redémarrer OnBoard), la disposition tmx devrait être apparue.

**Screenshot**
![Le clavier](/images/onboard_bepo.png)

